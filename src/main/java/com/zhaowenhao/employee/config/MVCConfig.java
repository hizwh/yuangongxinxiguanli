package com.zhaowenhao.employee.config;

import com.zhaowenhao.employee.interceptor.LoginHanlderInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;

/**
 * @Description: MVC配置类
 * @Author: zwh
 * @Date: 2019-06-28
 * @Time: 22:58
 */
@Configuration
public class MVCConfig implements WebMvcConfigurer {

    /**
     * 添加拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        ArrayList<String> list = new ArrayList<>();
        list.add("/");
        list.add("/login");
        list.add("/register");
        list.add("/user");
        registry.addInterceptor(new LoginHanlderInterceptor())
                .addPathPatterns("/**").excludePathPatterns(list);
    }

}
