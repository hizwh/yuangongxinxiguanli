package com.zhaowenhao.employee.constant;

/**
 * @Description:应用常量名
 * @Author: zwh
 * @Date: 2019-07-01
 * @Time: 17:12
 */
public class Constant {
    /**
     * 保存操作
     */
    public static String SAVE_STATUS = "save";

    /**
     * 更新操作
     */
    public static String UPDATE_STATUS = "update";


}
