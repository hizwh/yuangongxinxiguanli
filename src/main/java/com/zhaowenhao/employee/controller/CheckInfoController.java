package com.zhaowenhao.employee.controller;

import com.zhaowenhao.employee.pojo.Check;
import com.zhaowenhao.employee.pojo.Employee;
import com.zhaowenhao.employee.pojo.Message;
import com.zhaowenhao.employee.service.serviceImpl.CheckInfoServiceImpl;
import com.zhaowenhao.employee.service.serviceImpl.EmployeeInfoServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Description:考勤业务controller
 * @Author: zwh
 * @Date: 2019-06-30
 * @Time: 1:44
 */
@Controller
public class CheckInfoController {

    @Autowired
    CheckInfoServiceImpl checkInfoService;

    @Autowired
    EmployeeInfoServiceImpl employeeInfoService;

    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        //转换日期 注意这里的转化要和传进来的字符串的格式一直 如2015-9-9 就应该为yyyy-MM-dd
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        // CustomDateEditor为自定义日期编辑器
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat1, true));
    }

    @GetMapping("/check")
    @ResponseBody
    public Message getAllChecks(@Param("page") Integer page, @Param("limit") Integer limit) {
        List<Check> checks = checkInfoService.getAllChecks();
        if (checks.size() > 0) {
            Message message = Message.success();
            message.setData(checks);
            return message;
        }
        Message failed = Message.failed();
        failed.setMessage("数据为空！");
        return failed;
    }

    @PostMapping("/check")
    @ResponseBody
    public Message saveCheck(Check check) {
        System.out.println(check);
        Employee employeeById = employeeInfoService.findEmployeeById(check.getEmpId());
        if (employeeById == null) {
            Message message = Message.failed();
            message.setMessage("员工编号不存在，请重新输入！");
            return message;
        }
        boolean b = checkInfoService.saveCheck(check);
        return b ? Message.success() : Message.failed();
    }

    @DeleteMapping("/check")
    @ResponseBody
    public Message bentchDeleteCheckById(@Param("deleteId") String deleteId) {
        System.out.println(deleteId);
        String deleteStr = deleteId.substring(1, deleteId.length() - 2);
        String[] split = deleteStr.split(",");
        boolean b = checkInfoService.bentchDeleteCheckById(split);
        return b ? Message.success() : Message.failed();
    }

    @DeleteMapping("/check/{id}")
    @ResponseBody
    public Message deleteCheckById(@PathVariable("id") String id) {
        boolean b = checkInfoService.deleteCheckById(id);
        return b ? Message.success() : Message.failed();
    }

    @PutMapping("/check")
    @ResponseBody
    public Message updateCheckById(Check check) {
        System.out.println(check);
        boolean b = checkInfoService.updateCheckById(check);
        return b ? Message.success() : Message.failed();
    }


    @GetMapping("/updateCheck")
    public String updateCheck(@Param("id") String id, Model model) {
        Check check = checkInfoService.findCheckById(id);
        model.addAttribute("check", check);
        return "save_check";
    }

    @GetMapping("/showCheck")
    public String showCheckPage() {
        return "show_check";
    }

    @GetMapping("/saveCheck")
    public String showSaveCheck() {
        return "save_check";
    }

    @GetMapping("/searchChecks")
    @ResponseBody
    public Message searchChecks(@Param("queryWhere") String queryWhere, @Param("limit") String limit, @Param("page") String page) {
        List<Check> checks = checkInfoService.searchChecks(queryWhere);
        if (checks.size() > 0) {
            Message message = Message.success();
            message.setData(checks);
            return message;
        }
        Message message = Message.failed();
        message.setMessage("数据为空！");
        return message;
    }


}
