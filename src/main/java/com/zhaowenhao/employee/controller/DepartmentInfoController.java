package com.zhaowenhao.employee.controller;

import com.zhaowenhao.employee.pojo.Department;
import com.zhaowenhao.employee.pojo.Message;
import com.zhaowenhao.employee.service.serviceImpl.DepartmentInfoServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description:部门信息controller
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 14:50
 */
@Controller
public class DepartmentInfoController {

    @Autowired
    DepartmentInfoServiceImpl departmentInfoService;


    @GetMapping("/department")
    @ResponseBody
    public Message getAllDepartments() {
        List<Department> departments = departmentInfoService.getAllDepartments();
        if (departments.size() > 0) {
            Message message = Message.success();
            message.setData(departments);
            return message;
        }
        Message failed = Message.failed();
        failed.setMessage("数据为空！");
        return failed;
    }

    @PostMapping("/department")
    @ResponseBody
    public Message saveDepartment(Department department) {
        boolean b = departmentInfoService.saveDepartment(department);
        return b ? Message.success() : Message.failed();
    }

    @GetMapping("/searchDepartments")
    @ResponseBody
    public Message searchDepartments(@Param("queryWhere") String queryWhere, @Param("limit") String limit, @Param("page") String page) {
        List<Department> departments = departmentInfoService.searchDepartments(queryWhere);
        if (departments.size() > 0) {
            Message message = Message.success();
            message.setData(departments);
            return message;
        }
        Message message = Message.failed();
        message.setMessage("数据为空！");
        return message;
    }

    @GetMapping("/showDepartment")
    public String showDepartmentPage() {
        return "show_department";
    }

    @GetMapping("/saveDepartment")
    public String showSaveDepartment() {
        return "save_department";
    }

    @GetMapping("/updateDepartment")
    public String updateDepartmentPage(@Param("departId") String departId, Model model) {
        Department department = departmentInfoService.findDepartmentById(departId);
        model.addAttribute("department", department);
        return "save_department";
    }


    @PutMapping("/department")
    @ResponseBody
    public Message updateDepartmentById(Department department) {
        boolean b = departmentInfoService.updateDepartment(department);
        return b ? Message.success() : Message.failed();
    }

    @DeleteMapping("/department")
    @ResponseBody
    public Message bentchDeleteDepartments(@Param("deleteId") String deleteId) {
        String deleteStr = deleteId.substring(1, deleteId.length() - 2);
        String[] split = deleteStr.split(",");
        boolean b = departmentInfoService.bentchdeleteDepartmentByids(split);
        return b ? Message.success() : Message.failed();
    }

    @DeleteMapping("/department/{departId}")
    @ResponseBody
    public Message deleteEmployeeById(@PathVariable("departId") String departId) {
        boolean b = departmentInfoService.deleteDepartmentById(departId);
        return b ? Message.success() : Message.failed();
    }

}
