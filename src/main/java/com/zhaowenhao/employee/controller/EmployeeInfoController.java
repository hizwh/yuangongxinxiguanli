package com.zhaowenhao.employee.controller;

import com.zhaowenhao.employee.constant.Constant;
import com.zhaowenhao.employee.pojo.Employee;
import com.zhaowenhao.employee.pojo.Message;
import com.zhaowenhao.employee.service.serviceImpl.CheckInfoServiceImpl;
import com.zhaowenhao.employee.service.serviceImpl.EmployeeInfoServiceImpl;
import com.zhaowenhao.employee.service.serviceImpl.SalaryInfoServiceImpl;
import com.zhaowenhao.employee.utils.IO;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.UUID;

/**
 * @Description:员工信息处理器
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 13:00
 */
@Controller
public class EmployeeInfoController {

    @Autowired
    EmployeeInfoServiceImpl employeeInfoService;

    @Autowired
    SalaryInfoServiceImpl salaryInfoService;

    @Autowired
    CheckInfoServiceImpl checkInfoService;

    @ResponseBody
    @GetMapping("/employees")
    public Message getAllEmployees(@Param("page") Integer page, @Param("limit") Integer limit) {
        List<Employee> employeeList = employeeInfoService.getAllEmployees();
        if (employeeList.size() > 0) {
            Message message = Message.success();
            message.setData(employeeList);
            return message;
        }
        Message message = Message.failed();
        message.setMessage("数据为空！");
        return message;
    }

    @PostMapping("/employee")
    @ResponseBody
    public Message saveOrUpdateEmployee(@RequestParam("photo") MultipartFile photo, Employee employee, @Param("status") String status) {
        System.out.println(status);
        //修改操作
        if (Constant.UPDATE_STATUS.equals(status)) {
            return getMessage(photo, employee, employeeInfoService.updateEmployee(employee));
        } else {//保存操作
            Employee employeeById = employeeInfoService.findEmployeeById(employee.getEmpId());
            if (employeeById != null) {
                Message message = Message.failed();
                message.setMessage("员工编号已存在");
                return message;
            }
            return getMessage(photo, employee, employeeInfoService.saveEmployee(employee));
        }
    }

    /**
     * 通过保存或更新之后返回的状态message
     *
     * @param photo
     * @param employee
     * @param b2
     * @return
     */
    private Message getMessage(@RequestParam("photo") MultipartFile photo, Employee employee, boolean b2) {
        //判断是否有图片
        if (photo.getSize() != 0) {
            String path = IO.PHOTO_PATH + UUID.randomUUID().toString() + photo.getName();
            System.out.println(path);
            File file = new File(path);
            employee.setEmpPhoto(path);
            boolean a = IO.savePhoto(photo, file);
            boolean b = b2;
            return a && b ? Message.success() : Message.failed();
        } else {
            boolean a = b2;
            return a ? Message.success() : Message.failed();
        }
    }

    @DeleteMapping("/employee")
    @ResponseBody
    public Message bentchDeleteEmployees(@Param("deleteId") String deleteId) {
        String deleteStr = deleteId.substring(1, deleteId.length() - 2);
        String[] split = deleteStr.split(",");
        boolean b = checkInfoService.bentchDelteCheckByEmpId(split);
        boolean b1 = salaryInfoService.bentchDeleteSalariesByEmpId(split);
        boolean b2 = employeeInfoService.bentchDeleteEmployees(split);
        return b && b1 && b2 ? Message.success() : Message.failed();
    }

    @DeleteMapping("/employee/{empId}")
    @ResponseBody
    public Message deleteEmployeeById(@PathVariable("empId") String empId) {
        boolean b = checkInfoService.deleteCheckByEmpId(empId);
        boolean b1 = salaryInfoService.deleteSalaryByEmpId(empId);
        boolean b2 = employeeInfoService.deleteEmployeeById(empId);
        return b && b1 && b2 ? Message.success() : Message.failed();
    }

    @GetMapping("/updateEmp")
    public String updateEmp(@Param("empId") String empId, Model model) {
        Employee employee = employeeInfoService.findEmployeeById(empId);
        model.addAttribute("emp", employee);
        return "save_employee";
    }

    @GetMapping("/saveEmp")
    public String showSaveEmployee() {
        return "save_employee";
    }

    @GetMapping("/showEmp")
    public String showEmployeePage() {
        return "show_employee";
    }

    @GetMapping("/searchEmp")
    @ResponseBody
    public Message searchEmp(@Param("queryWhere") String queryWhere) {
        List<Employee> employeeList = employeeInfoService.searchEmployees(queryWhere);
        if (employeeList.size() > 0) {
            Message message = Message.success();
            message.setData(employeeList);
            return message;
        }
        Message message = Message.failed();
        message.setMessage("数据为空！");
        return message;
    }

}
