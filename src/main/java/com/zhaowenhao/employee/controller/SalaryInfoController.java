package com.zhaowenhao.employee.controller;

import com.zhaowenhao.employee.pojo.Employee;
import com.zhaowenhao.employee.pojo.Message;
import com.zhaowenhao.employee.pojo.Salary;
import com.zhaowenhao.employee.service.serviceImpl.EmployeeInfoServiceImpl;
import com.zhaowenhao.employee.service.serviceImpl.SalaryInfoServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description:基本薪资处理器controller
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 16:22
 */
@Controller
public class SalaryInfoController {

    @Autowired
    SalaryInfoServiceImpl salaryInfoService;

    @Autowired
    EmployeeInfoServiceImpl employeeInfoService;

    @GetMapping("/salary")
    @ResponseBody
    public Message getAllSalaries() {
        List<Salary> salaries = salaryInfoService.getAllSalaries();
        if (salaries.size() > 0) {
            Message message = Message.success();
            message.setData(salaries);
            return message;
        }
        Message failed = Message.failed();
        failed.setMessage("数据为空！");
        return failed;
    }

    @PostMapping("/salary")
    @ResponseBody
    public Message saveSalary(Salary salary) {
        Salary salaryById = salaryInfoService.findSalaryById(salary.getEmpId());
        if (salaryById != null) {
            Message message = Message.failed();
            message.setMessage("工资信息已存在，不可进行添加操作！");
            return message;
        } else {
            Employee employeeById = employeeInfoService.findEmployeeById(salary.getEmpId());
            if (employeeById == null) {
                Message message = Message.failed();
                message.setMessage("不存在的员工编号,请重试！");
                return message;
            } else {
                boolean b = salaryInfoService.saveSalary(salary);
                return b ? Message.success() : Message.failed();
            }
        }
    }

    @DeleteMapping("/salary")
    @ResponseBody
    public Message bentchDeleteSalariesByEmpId(@Param("deleteId") String deleteId) {
        System.out.println(deleteId);
        String deleteStr = deleteId.substring(1, deleteId.length() - 2);
        String[] split = deleteStr.split(",");
        boolean b = salaryInfoService.bentchDeleteSalariesByEmpId(split);
        return b ? Message.success() : Message.failed();
    }

    @DeleteMapping("/salary/{empId}")
    @ResponseBody
    public Message deleteEmployeeById(@PathVariable("empId") String empId) {
        boolean b = salaryInfoService.deleteSalaryByEmpId(empId);
        return b ? Message.success() : Message.failed();
    }


    @PutMapping("/salary")
    @ResponseBody
    public Message updateSalaryById(Salary salary) {
        boolean b = salaryInfoService.updateSalaryByEmpId(salary);
        return b ? Message.success() : Message.failed();
    }

    @GetMapping("/showSalaries")
    public String showSalariesPage() {
        return "show_salary";
    }

    @GetMapping("/saveSalaries")
    public String showSaveSalaries() {
        return "save_salary";
    }

    @GetMapping("/updateSalary")
    public String updateEmp(@Param("empId") String empId, Model model) {
        Salary salary = salaryInfoService.findSalaryById(empId);
        model.addAttribute("salary", salary);
        return "save_salary";
    }

    @GetMapping("/searchSalaries")
    @ResponseBody
    public Message searchSalaries(@Param("queryWhere") String queryWhere, @Param("limit") String limit, @Param("page") String page) {
        List<Salary> salaries = salaryInfoService.searchSalaries(queryWhere);
        if (salaries.size() > 0) {
            Message message = Message.success();
            message.setData(salaries);
            return message;
        }
        Message message = Message.failed();
        message.setMessage("数据为空！");
        return message;

    }
}
