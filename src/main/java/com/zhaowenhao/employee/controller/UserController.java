package com.zhaowenhao.employee.controller;

import com.zhaowenhao.employee.pojo.Message;
import com.zhaowenhao.employee.pojo.User;
import com.zhaowenhao.employee.service.serviceImpl.UserServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;

/**
 * Description:登录controller
 * Author: zwh
 * Date: 2019-06-28
 * Time: 19:31
 */
@Controller
public class UserController {

    @Autowired
    UserServiceImpl userService;

    @GetMapping(value = {"/login", "/"})
    public String loginBefore() {
        return "login";
    }

    @GetMapping(value = {"/register"})
    public String register() {
        return "register";
    }

    @GetMapping(value = {"/main"})
    public String main() {
        return "EmployeeMain";
    }

    @PostMapping(value = "/user")
    public String login(User user, Model model, HttpSession session) {
        boolean b = userService.checkUserName(user.getUserName());
        boolean loginResult = userService.login(user);
        if (b) {
            if (!loginResult) {
                Message message = Message.failed();
                message.setMessage("密码错误");
                model.addAttribute("message", message);
                return "login";
            } else {
                session.setAttribute("user", user.getUserName());
                return "EmployeeMain";
            }
        } else {
            Message message = Message.failed();
            message.setMessage("该用户不存在，请注册！");
            model.addAttribute("message", message);
            return "login";
        }
    }

    @PostMapping(value = "/register")
    public String register(User user, @Param("userPassAgain") String userPassAgain, Model model, HttpSession session) {
        if (!userPassAgain.equals(user.getUserPass())) {
            Message message = Message.failed();
            message.setMessage("两次密码输入不一致！");
            model.addAttribute("message", message);
        } else {
            boolean b = userService.checkUserName(user.getUserName());
            if (b) {
                Message message = Message.failed();
                message.setMessage("该用户名已被占用！");
                model.addAttribute("message", message);
            } else {
                boolean b1 = userService.saveUser(user);
                if (b1) {
                    Message message = Message.success();
                    message.setMessage("注册成功！");
                    model.addAttribute("message", message);
                    session.setAttribute("user", user.getUserName());
                }
            }
        }
        return "register";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.removeAttribute("user");
        return "redirect:/login";
    }

    @GetMapping("/update_password")
    public String showUpdatePassword() {
        return "update_password";
    }

    @PostMapping("/update_password")
    public String updatePassword(User user, @Param("newPass") String newPass, Model model, HttpSession session) {
        if (newPass.equals(user.getUserPass())) {
            System.out.println("新旧");
            Message message = Message.failed();
            message.setMessage("新旧密码不可以相同！");
            model.addAttribute("message", message);
            return "update_password";
        }

        boolean b = userService.login(user);
        if (b) {
            user.setUserPass(newPass);
            boolean b1 = userService.updatePassByUsername(user);
            Message message = Message.success();
            model.addAttribute("message", message);
            if (b1) {
                System.out.println("更新成功");
                return "update_password";
            }
        }
        System.out.println("失败");
        Message message = Message.failed();
        message.setMessage("旧密码输入错误！");
        model.addAttribute("message", message);
        return "update_password";
    }


}
