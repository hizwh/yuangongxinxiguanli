package com.zhaowenhao.employee.dao;

import com.zhaowenhao.employee.pojo.Check;
import com.zhaowenhao.employee.provider.SqlProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Description:考勤类DAO层
 * Author: zwh
 * Date: 2019-06-28
 * Time: 19:21
 */
@Mapper
@Component
public interface CheckInfoDao {
    String TABLE_NAME = "employeesystem.check_emp";
    String TABLE_FIELD = " (emp_id,check_date,check_type)";

    @Insert({"insert into ", TABLE_NAME, TABLE_FIELD, "values (#{empId},#{checkDate},#{checkType})"})
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    Integer saveCheck(Check check);

    @Delete({"delete from ", TABLE_NAME, "where id = #{id}"})
    Integer deleteCheckById(@Param("id") String id);

    @Delete({"delete from ", TABLE_NAME, "where emp_id = #{empId}"})
    Integer deleteCheckByEmpId(@Param("empId") String empId);

    @DeleteProvider(type = SqlProvider.class, method = "bentchDeleteCheckById")
    Integer bentchDeleteCheckById(@Param("ids") String[] ids);

    @DeleteProvider(type = SqlProvider.class, method = "bentchDeleteCheckByEmpId")
    Integer bentchDeleteCheckByEmpId(@Param("empIds") String[] empIds);

    @Select({"select * from ", TABLE_NAME})
    List<Check> getAllChecks();

    @Select({"select * from ", TABLE_NAME, "where id = #{id}"})
    Check findCheckById(@Param("id") String id);

    @Update({"update ", TABLE_NAME, " set emp_id =#{empId}, check_date = #{checkDate},check_type = #{checkType} where id =#{id}"})
    Integer updateCheckById(Check check);

    @SelectProvider(type = SqlProvider.class, method = "searchChecks")
    List<Check> searchChecks(String queryWhere);
}
