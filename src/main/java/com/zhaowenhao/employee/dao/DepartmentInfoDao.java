package com.zhaowenhao.employee.dao;

import com.zhaowenhao.employee.pojo.Department;
import com.zhaowenhao.employee.provider.SqlProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 14:43
 */
@Mapper
@Component
public interface DepartmentInfoDao {

    String TABLE_NAME = "employeesystem.department";


    @Select({"select * from ", TABLE_NAME})
    List<Department> getAllDepartments();

    @Select({"select * from ", TABLE_NAME, " where depart_id = #{queryWhere}"})
    List<Department> searchDepartments(@Param("queryWhere") String queryWhere);

    @Select({"select * from ", TABLE_NAME, " where depart_id = #{departId}"})
    Department findDepartmentById(@Param("departId") String departId);

    @Select({"select * from ", TABLE_NAME, " where depart_name = #{departName}"})
    Department findDepartmentByName(String departName);

    @Insert({"insert into ", TABLE_NAME, "values(#{departId},#{departName})"})
    Integer saveDepartment(Department department);

    @Delete({"delete from ", TABLE_NAME, "where depart_id = #{departId}"})
    Integer deleteDepartmentById(@Param("departId") String departId);

    @DeleteProvider(type = SqlProvider.class, method = "bentchDeleteDepartmentsById")
    Integer bentchDeleteDepartmentsById(@Param("departIds") String[] departIds);

    @Update({"update ",TABLE_NAME," set depart_name = #{departName} where depart_id = #{departId}"})
    Integer updateDepartmentById(Department department);

}
