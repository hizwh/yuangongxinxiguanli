package com.zhaowenhao.employee.dao;

import com.zhaowenhao.employee.pojo.Employee;
import com.zhaowenhao.employee.provider.SqlProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:员工基本信息处理器
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 13:01
 */
@Mapper
@Component
public interface EmployeeInfoDao {

    String TABLE_NAME = "employeesystem.employee";

    @Select({"select * from", TABLE_NAME})
    List<Employee> getAllEmployees();

    @SelectProvider(type = SqlProvider.class, method = "searchEmployees")
    List<Employee> searchEmployees(@Param("queryWhere") String queryWhere);

    @Select({"select * from ", TABLE_NAME, " where emp_id = #{empId}"})
    Employee findEmployeeById(@Param("empId") String empId);

    @Insert({"insert into", TABLE_NAME, "values(#{empId},#{departId},#{empName},#{empAge},#{empSex},#{empTel},#{empAddr},#{empPhoto})"})
    Integer saveEmployee(Employee employee);

    @Delete({"delete from", TABLE_NAME, "where emp_id = #{empId}"})
    Integer deleteEmployeeById(String empId);

    @DeleteProvider(type = SqlProvider.class, method = "bentchDeleteEmployeesById")
    Integer bentchDeleteEmployeesById(@Param("empIds") String[] empIds);

    @Update({"update ", TABLE_NAME, " set depart_id = #{departId} , emp_name = #{empName} ," +
            "emp_age = #{empAge} , emp_sex = #{empSex}, emp_tel = #{empTel},emp_addr =#{empAddr} ,emp_photo = #{empPhoto} where emp_id = #{empId}"})
    Integer updateEmployeeById(Employee employee);

}
