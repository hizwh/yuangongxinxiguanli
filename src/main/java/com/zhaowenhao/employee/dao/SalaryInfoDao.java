package com.zhaowenhao.employee.dao;

import com.zhaowenhao.employee.pojo.Salary;
import com.zhaowenhao.employee.provider.SqlProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:薪资信息Dao
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 16:14
 */
@Mapper
@Component
public interface SalaryInfoDao {
    String TABLE_NAME = "employeesystem.salary";

    @Select({"select * from", TABLE_NAME})
    List<Salary> getAllSalaries();

    @Select({"select * from", TABLE_NAME, " where emp_id = #{empId}"})
    Salary findSalaryByEmpId(@Param("empId") String empId);

    @Select({"select * from ", TABLE_NAME, " where emp_id = #{queryWhere}"})
    List<Salary> searchSalaries(@Param("queryWhere") String queryWhere);

    @Insert({"insert into", TABLE_NAME, "values(#{empId},#{empBase},#{empBonus})"})
    Integer saveSalary(Salary salary);

    @Delete({"delete from ", TABLE_NAME, "where emp_id = #{empId}"})
    Integer deleteSalaryByEmpId(@Param("empId") String empId);

    @DeleteProvider(type = SqlProvider.class, method = "bentchDeleteSalariesByEmpId")
    Integer bentchDeleteSalariesByEmpId(@Param("empIds") String[] empIds);

    @Update({"update ", TABLE_NAME, " set emp_base = #{empBase},emp_bonus = #{empBonus} where emp_id = #{empId}"})
    Integer updateEmployeeById(Salary salary);
}
