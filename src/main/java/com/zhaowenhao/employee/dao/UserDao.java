package com.zhaowenhao.employee.dao;

import com.zhaowenhao.employee.pojo.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * Description:用户层DAO
 * Author: zwh
 * Date: 2019-06-28
 * Time: 18:50
 */
@Mapper
@Component
public interface UserDao {

    String TABLE_NAME = "employeesystem.user";

    @Select({"select * from", TABLE_NAME, " where user_name = #{userName}"})
    User checkUserByName(@Param("userName") String userName);

    @Select({"select * from", TABLE_NAME, " where user_name = #{userName} and user_pass = #{userPass}"})
    User findUser(@Param("userName") String userName, @Param("userPass") String userPass);

    @Insert({"insert into ", TABLE_NAME, "values ( #{userName} , #{userPass})"})
    Integer saveUser(User user);

    @Delete({"delete from ", TABLE_NAME, "where user_name = #{userName}"})
    int deleteUser(@Param("userName") String userName);

    @Update({"update ", TABLE_NAME, " set user_pass = #{userPass} where user_name = #{userName}"})
    Integer updatePassByUserName(User user);
}
