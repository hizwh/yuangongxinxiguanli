package com.zhaowenhao.employee.pojo;

import java.util.Date;

/**
 * Description:员工考勤类
 * User: zwh
 * Date: 2019-06-28
 * Time: 18:30
 */

public class Check {

    /**
     * 对应自增主键
     */
    private Integer id;
    /**
     * 员工工号
     */
    private String empId;

    /**
     * 员工考勤时间
     */
    private Date checkDate;

    /**
     * 考勤类型
     */
    private String checkType;

    public Check() {
    }

    public Check(Integer id, String empId, Date checkDate, String checkType) {
        this.id = id;
        this.empId = empId;
        this.checkDate = checkDate;
        this.checkType = checkType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    @Override
    public String toString() {
        return "Check{" +
                "id=" + id +
                ", empId='" + empId + '\'' +
                ", checkDate=" + checkDate +
                ", checkType='" + checkType + '\'' +
                '}';
    }
}
