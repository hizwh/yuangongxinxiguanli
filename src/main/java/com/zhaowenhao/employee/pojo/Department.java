package com.zhaowenhao.employee.pojo;

/**
 * Description:部门信息类
 * User: zwh
 * Date: 2019-06-28
 * Time: 18:32
 */
public class Department {

    /**
     * 部门id
     */
    private String departId;

    /**
     * 部门名称
     */
    private String departName;

    public Department() {
    }

    public Department(String departId, String departName) {
        this.departId = departId;
        this.departName = departName;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    public String getDepartName() {
        return departName;
    }

    public void setDepartName(String departName) {
        this.departName = departName;
    }

    @Override
    public String toString() {
        return "Department{" +
                "departId='" + departId + '\'' +
                ", departName='" + departName + '\'' +
                '}';
    }


}
