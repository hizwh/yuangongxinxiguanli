package com.zhaowenhao.employee.pojo;

/**
 * Description:员工基本信息类
 * User: zwh
 * Date: 2019-06-28
 * Time: 18:39
 */
public class Employee {

    /**
     * 员工工号
     */
    private String empId;

    /**
     * 员工所在部门编号
     */
    private String departId;

    /**
     * 员工姓名
     */
    private String empName;

    /**
     * 员工年龄
     */
    private Integer empAge;

    /**
     * 员工性别
     */
    private String empSex;

    /**
     * 员工联系方式
     */
    private String empTel;

    /**
     * 员工家庭住址
     */
    private String empAddr;

    /**
     * 员工上传图片路径
     */
    private String empPhoto;

    public Employee() {
    }

    public Employee(String empId, String departId, String empName, Integer empAge, String empSex, String empTel, String empAddr, String empPhoto) {
        this.empId = empId;
        this.departId = departId;
        this.empName = empName;
        this.empAge = empAge;
        this.empSex = empSex;
        this.empTel = empTel;
        this.empAddr = empAddr;
        this.empPhoto = empPhoto;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public Integer getEmpAge() {
        return empAge;
    }

    public void setEmpAge(Integer empAge) {
        this.empAge = empAge;
    }

    public String getEmpSex() {
        return empSex;
    }

    public void setEmpSex(String empSex) {
        this.empSex = empSex;
    }

    public String getEmpTel() {
        return empTel;
    }

    public void setEmpTel(String empTel) {
        this.empTel = empTel;
    }

    public String getEmpAddr() {
        return empAddr;
    }

    public void setEmpAddr(String empAddr) {
        this.empAddr = empAddr;
    }

    public String getEmpPhoto() {
        return empPhoto;
    }

    public void setEmpPhoto(String empPhoto) {
        this.empPhoto = empPhoto;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "empId='" + empId + '\'' +
                ", departId='" + departId + '\'' +
                ", empName='" + empName + '\'' +
                ", empAge=" + empAge +
                ", empSex='" + empSex + '\'' +
                ", empTel='" + empTel + '\'' +
                ", empAddr='" + empAddr + '\'' +
                ", empPhoto='" + empPhoto + '\'' +
                '}';
    }
}
