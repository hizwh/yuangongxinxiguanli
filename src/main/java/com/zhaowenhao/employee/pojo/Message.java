package com.zhaowenhao.employee.pojo;

/**
 * @Description:状态消息bean
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 14:12
 */
public class Message {

    /**
     * 状态码
     */
    private Integer code;

    /**
     * 状态消息
     */
    private String message;

    /**
     * 附加数据
     */
    private Object data;

    public Message() {
    }

    public Message(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static Message success() {
        Message message = new Message();
        message.code = 0;
        message.setData(null);
        message.setMessage("成功！");
        return message;
    }


    public static Message failed() {
        Message message = new Message();
        message.code = -1;
        message.setData(null);
        message.setMessage("失败！");
        return message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Message{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
