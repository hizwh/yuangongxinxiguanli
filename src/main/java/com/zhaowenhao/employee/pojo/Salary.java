package com.zhaowenhao.employee.pojo;

/**
 * Description:员工工资类
 * User: zwh
 * Date: 2019-06-28
 * Time: 18:35
 */
public class Salary {
    /**
     * 员工工号
     */
    private String empId;

    /**
     * 员工基本工资
     */
    private Double empBase;

    /**
     * 员工奖金
     */
    private Double empBonus;

    public Salary() {
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public Double getEmpBase() {
        return empBase;
    }

    public void setEmpBase(Double empBase) {
        this.empBase = empBase;
    }

    public Double getEmpBonus() {
        return empBonus;
    }

    public void setEmpBonus(Double empBonus) {
        this.empBonus = empBonus;
    }

    @Override
    public String toString() {
        return "Salary{" +
                "empId='" + empId + '\'' +
                ", empBase=" + empBase +
                ", empBonus=" + empBonus +
                '}';
    }
}
