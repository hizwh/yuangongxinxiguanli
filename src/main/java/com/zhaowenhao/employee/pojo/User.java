package com.zhaowenhao.employee.pojo;

/**
 * Description:管理员信息类
 * User: zwh
 * Date: 2019-06-28
 * Time: 18:37
 */
public class User {
    /**
     * 管理员用户名
     */
    private String userName;

    /**
     * 管理员密码
     */
    private String userPass;

    public User() {
    }

    public User(String userName, String userPass) {
        this.userName = userName;
        this.userPass = userPass;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", userPass='" + userPass + '\'' +
                '}';
    }
}
