package com.zhaowenhao.employee.provider;

import com.zhaowenhao.employee.utils.SQL;
import org.apache.ibatis.annotations.Param;

/**
 * @Description:sql provider实现复杂sql操作
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 19:14
 */

public class SqlProvider {
    /**
     * 批量删除员工
     *
     * @param empIds
     * @return
     */
    public String bentchDeleteEmployeesById(@Param("empIds") String[] empIds) {
        String sql = "delete from employeesystem.employee where emp_id in (";
        return SQL.getSQL(empIds, sql);
    }


    /**
     * 批量删除考勤记录
     *
     * @param empIds
     * @return
     */
    public String bentchDeleteCheckByEmpId(@Param("empIds") String[] empIds) {
        String sql = "delete from employeesystem.check_emp where emp_id in (";
        return SQL.getSQL(empIds, sql);
    }

    /**
     * 批量删除工资
     *
     * @param empIds
     * @return
     */
    public String bentchDeleteSalariesByEmpId(@Param("empIds") String[] empIds) {
        String sql = "delete from employeesystem.salary where emp_id in (";
        return SQL.getSQL(empIds, sql);
    }

    /**
     * 批量删除考勤通过id
     *
     * @param ids
     * @return
     */
    public String bentchDeleteCheckById(@Param("ids") String[] ids) {
        String sql = "delete from employeesystem.check_emp where id in (";
        return SQL.getSQL(ids, sql);
    }

    /**
     * 批量删除部门通过id
     * (注意：级联强制删除拥有此外键的记录)
     * @param departIds
     * @return
     */
    public String bentchDeleteDepartmentsById(@Param("departIds") String[] departIds){
        String sql = "delete from employeesystem.department where depart_id in (";
        return SQL.getSQL(departIds, sql);
    }


    /**
     * 按指定条件模糊搜索员工信息
     *
     * @param queryWhere
     * @return
     */
    public String searchEmployees(@Param("queryWhere") String queryWhere) {
        String sql = "select * from employeesystem.employee where emp_id ='" + queryWhere
                + "' or emp_name like '%" + queryWhere + "%'";
        return sql;
    }

    /**
     * 按指定条件模糊搜索考勤信息
     *
     * @param queryWhere
     * @return
     */
    public String searchChecks(@Param("queryWhere") String queryWhere) {
        String sql = "select * from employeesystem.check_emp where emp_id ='" + queryWhere
                + "' or check_type like '%" + queryWhere + "%'";
        return sql;
    }
}
