package com.zhaowenhao.employee.service;

import com.zhaowenhao.employee.pojo.Check;

import java.util.List;

/**
 * @Description:
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 21:56
 */
public interface CheckInfoService {

    /**
     * 保存考勤信息
     *
     * @param check
     * @return
     */
    boolean saveCheck(Check check);

    /**
     * 查询所有考勤信息
     *
     * @return
     */
    List<Check> getAllChecks();

    /**
     * 通过id编号删除考勤信息
     *
     * @param id
     * @return
     */
    boolean deleteCheckById(String id);

    /**
     * 通过员工工号删除考勤信息
     *
     * @param empId
     * @return
     */
    boolean deleteCheckByEmpId(String empId);

    /**
     * 通过id编号批量删除考勤信息
     *
     * @param ids
     * @return
     */
    boolean bentchDeleteCheckById(String[] ids);

    /**
     * 通过id编号查找考勤信息
     *
     * @param id
     * @return
     */
    Check findCheckById(String id);

    /**
     * 通过id编号修改考勤信息
     *
     * @param check
     * @return
     */
    boolean updateCheckById(Check check);

    /**
     * 通过员工工号批量删除考勤信息
     *
     * @param empIds
     * @return
     */
    boolean bentchDelteCheckByEmpId(String[] empIds);

    /**
     * 按照指定条件查询考勤信息
     *
     * @param empId
     * @return
     */
    List<Check> searchChecks(String empId);
}
