package com.zhaowenhao.employee.service;

import com.zhaowenhao.employee.pojo.Department;

import java.util.List;

/**
 * @Description:员工部门信息业务
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 14:46
 */

public interface DepartmentInfoService {

    /**
     * 查询所有的部门信息
     *
     * @return
     */
    List<Department> getAllDepartments();

    /**
     * 通过指定条件查询部门信息
     *
     * @param queryWhere
     * @return
     */
    List<Department> searchDepartments(String queryWhere);

    /**
     * 保存部门信息
     *
     * @param department
     * @return
     */
    boolean saveDepartment(Department department);

    boolean updateDepartment(Department department);

    boolean deleteDepartmentById(String departId);

    boolean bentchdeleteDepartmentByids(String[] departId);

    Department findDepartmentById(String departId);

    Department findDepartmentByName(String departName);

}
