package com.zhaowenhao.employee.service;

import com.zhaowenhao.employee.pojo.Employee;

import java.util.List;

/**
 * @Description:员工基本信息业务
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 13:44
 */
public interface EmployeeInfoService {

    /**
     * 查询所有的员工
     *
     * @return
     */
    List<Employee> getAllEmployees();

    /**
     * 通过员工工号查询员工信息
     *
     * @param empId
     * @return
     */
    Employee findEmployeeById(String empId);

    /**
     * 保存员工信息
     *
     * @param employee
     * @return
     */
    boolean saveEmployee(Employee employee);

    /**
     * 通过员工工号删除员工信息
     *
     * @param empId
     * @return
     */
    boolean deleteEmployeeById(String empId);

    /**
     * 通过员工工号批量删除员工信息
     *
     * @param empIds
     * @return
     */
    boolean bentchDeleteEmployees(String[] empIds);

    /**
     * 通过员工工号修改员工信息
     *
     * @param employee
     * @return
     */
    boolean updateEmployee(Employee employee);

    /**
     * 按指定条件查询员工信息
     *
     * @param queryWhere
     * @return
     */
    List<Employee> searchEmployees(String queryWhere);
}
