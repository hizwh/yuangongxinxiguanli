package com.zhaowenhao.employee.service;

import com.zhaowenhao.employee.pojo.Salary;

import java.util.List;

/**
 * @Description:
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 16:17
 */
public interface SalaryInfoService {
    /**
     * 查询所有工资信息
     *
     * @return
     */
    List<Salary> getAllSalaries();

    /**
     * 通过id编号查找工资
     *
     * @param empId
     * @return
     */
    Salary findSalaryById(String empId);

    /**
     * 通过指定条件查询所有符合条件的工资信息
     *
     * @param queryWhere
     * @return
     */
    List<Salary> searchSalaries(String queryWhere);

    /**
     * 保存工资信息
     *
     * @param salary
     * @return
     */
    boolean saveSalary(Salary salary);

    /**
     * 通过员工工号删除工资信息
     *
     * @param empId
     * @return
     */
    boolean deleteSalaryByEmpId(String empId);

    /**
     * 通过员工工号批量删除工资信息
     *
     * @param empIds
     * @return
     */
    boolean bentchDeleteSalariesByEmpId(String[] empIds);

    /**
     * 通过员工工号修改工资信息
     *
     * @param salary
     * @return
     */
    boolean updateSalaryByEmpId(Salary salary);
}
