package com.zhaowenhao.employee.service;

import com.zhaowenhao.employee.pojo.User;

/**
 * Description:后台管理员登录业务
 * Author: zwh
 * Date: 2019-06-28
 * Time: 19:34
 */
public interface UserService {

    /**
     * 登录功能
     */
    boolean login(User user);

    /**
     * 查询用户名是否被占用
     */
    boolean checkUserName(String userName);

    /**
     * 保存用户
     */
    boolean saveUser(User user);

    boolean updatePassByUsername(User user);
}
