package com.zhaowenhao.employee.service.serviceImpl;

import com.zhaowenhao.employee.dao.CheckInfoDao;
import com.zhaowenhao.employee.pojo.Check;
import com.zhaowenhao.employee.service.CheckInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 21:57
 */

@Service
public class CheckInfoServiceImpl implements CheckInfoService {

    @Autowired
    CheckInfoDao checkInfoDao;


    @Override
    public boolean saveCheck(Check check) {
        Integer integer = checkInfoDao.saveCheck(check);
        return integer != null;
    }

    @Override
    public List<Check> getAllChecks() {
        List<Check> checks = checkInfoDao.getAllChecks();
        return checks;
    }


    @Override
    public boolean deleteCheckById(String id) {
        Integer integer = checkInfoDao.deleteCheckById(id);
        return integer != null;
    }

    @Override
    public boolean deleteCheckByEmpId(String empId) {
        Integer integer = checkInfoDao.deleteCheckByEmpId(empId);
        return integer != null;
    }


    @Override
    public boolean bentchDeleteCheckById(String[] ids) {
        Integer integer = checkInfoDao.bentchDeleteCheckById(ids);
        return integer != null;
    }

    @Override
    public Check findCheckById(String id) {
        Check check = checkInfoDao.findCheckById(id);
        return check;
    }

    @Override
    public boolean updateCheckById(Check check) {
        Integer integer = checkInfoDao.updateCheckById(check);
        return integer != null;
    }

    @Override
    public boolean bentchDelteCheckByEmpId(String[] empIds) {
        Integer integer = checkInfoDao.bentchDeleteCheckByEmpId(empIds);
        return integer != null;
    }

    @Override
    public List<Check> searchChecks(String queryWhere) {
        List<Check> checks = checkInfoDao.searchChecks(queryWhere);
        return checks;
    }
}
