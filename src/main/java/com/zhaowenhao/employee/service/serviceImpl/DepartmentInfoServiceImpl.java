package com.zhaowenhao.employee.service.serviceImpl;

import com.zhaowenhao.employee.dao.DepartmentInfoDao;
import com.zhaowenhao.employee.pojo.Department;
import com.zhaowenhao.employee.service.DepartmentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 14:49
 */
@Service
public class DepartmentInfoServiceImpl implements DepartmentInfoService {

    @Autowired
    DepartmentInfoDao departmentInfoDao;

    @Override
    public List<Department> getAllDepartments() {
        List<Department> departments = departmentInfoDao.getAllDepartments();
        return departments;
    }

    @Override
    public List<Department> searchDepartments(String queryWhere) {
        List<Department> departments = departmentInfoDao.searchDepartments(queryWhere);
        return departments;
    }

    @Override
    public boolean saveDepartment(Department department) {
        Integer integer = departmentInfoDao.saveDepartment(department);
        return integer != null;
    }

    @Override
    public boolean updateDepartment(Department department) {
        Integer integer = departmentInfoDao.updateDepartmentById(department);
        return integer != null;
    }

    @Override
    public boolean deleteDepartmentById(String departId) {
        Integer integer = departmentInfoDao.deleteDepartmentById(departId);
        return integer != null;
    }

    @Override
    public boolean bentchdeleteDepartmentByids(String[] departId) {
        Integer integer = departmentInfoDao.bentchDeleteDepartmentsById(departId);
        return integer!=null;
    }

    @Override
    public Department findDepartmentById(String departId) {
        Department departmentById = departmentInfoDao.findDepartmentById(departId);
        return departmentById;
    }

    @Override
    public Department findDepartmentByName(String departName) {
        Department departmentByName = departmentInfoDao.findDepartmentByName(departName);
        return departmentByName;
    }
}
