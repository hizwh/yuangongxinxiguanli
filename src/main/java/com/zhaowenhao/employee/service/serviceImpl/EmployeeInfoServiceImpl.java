package com.zhaowenhao.employee.service.serviceImpl;

import com.zhaowenhao.employee.dao.EmployeeInfoDao;
import com.zhaowenhao.employee.pojo.Employee;
import com.zhaowenhao.employee.service.EmployeeInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 13:46
 */
@Service
public class EmployeeInfoServiceImpl implements EmployeeInfoService {

    @Autowired
    EmployeeInfoDao employeeInfoDao;


    @Override
    public List<Employee> getAllEmployees() {
        return employeeInfoDao.getAllEmployees();
    }

    @Override
    public Employee findEmployeeById(String empId) {
        return employeeInfoDao.findEmployeeById(empId);
    }

    @Override
    public boolean saveEmployee(Employee employee) {
        Integer integer = employeeInfoDao.saveEmployee(employee);
        return integer != null;
    }

    @Override
    public boolean deleteEmployeeById(String empId) {
        Integer integer = employeeInfoDao.deleteEmployeeById(empId);
        return integer != null;
    }

    @Override
    public boolean bentchDeleteEmployees(String[] empIds) {
        Integer integer = employeeInfoDao.bentchDeleteEmployeesById(empIds);
        return integer != null;
    }

    @Override
    public boolean updateEmployee(Employee employee) {
        Integer integer = employeeInfoDao.updateEmployeeById(employee);
        return integer != null;
    }

    @Override
    public List<Employee> searchEmployees(String queryWhere) {
        List<Employee> employeeList = employeeInfoDao.searchEmployees(queryWhere);
        for (int i = 0; i < employeeList.size(); i++) {
            System.out.println(employeeList.get(i));
        }
        return employeeList;
    }
}
