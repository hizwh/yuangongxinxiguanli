package com.zhaowenhao.employee.service.serviceImpl;

import com.zhaowenhao.employee.dao.SalaryInfoDao;
import com.zhaowenhao.employee.pojo.Salary;
import com.zhaowenhao.employee.service.SalaryInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Author: zwh
 * @Date: 2019-06-29
 * @Time: 16:18
 */
@Service
public class SalaryInfoServiceImpl implements SalaryInfoService {

    @Autowired
    SalaryInfoDao salaryInfoDao;

    @Override
    public List<Salary> getAllSalaries() {
        List<Salary> salaries = salaryInfoDao.getAllSalaries();
        return salaries;
    }

    @Override
    public Salary findSalaryById(String empId) {
        Salary salary = salaryInfoDao.findSalaryByEmpId(empId);
        return salary;
    }

    @Override
    public List<Salary> searchSalaries(String queryWhere) {
        List<Salary> salaries = salaryInfoDao.searchSalaries(queryWhere);
        return salaries;
    }

    @Override
    public boolean saveSalary(Salary salary) {
        Integer integer = salaryInfoDao.saveSalary(salary);
        return integer != null;
    }

    @Override
    public boolean deleteSalaryByEmpId(String empId) {
        Integer integer = salaryInfoDao.deleteSalaryByEmpId(empId);
        return integer != null;
    }

    @Override
    public boolean bentchDeleteSalariesByEmpId(String[] empIds) {
        Integer integer = salaryInfoDao.bentchDeleteSalariesByEmpId(empIds);
        return integer != null;
    }

    @Override
    public boolean updateSalaryByEmpId(Salary salary) {
        Integer integer = salaryInfoDao.updateEmployeeById(salary);
        return integer != null;
    }

}
