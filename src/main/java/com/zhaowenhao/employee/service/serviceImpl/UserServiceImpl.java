package com.zhaowenhao.employee.service.serviceImpl;

import com.zhaowenhao.employee.dao.UserDao;
import com.zhaowenhao.employee.pojo.User;
import com.zhaowenhao.employee.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description:UserService实现类
 * @Author: zwh
 * @Date: 2019-06-28
 * @Time: 19:35
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public boolean login(User user) {
        User result = userDao.findUser(user.getUserName(), user.getUserPass());
        System.out.println(result);
        if (result != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean checkUserName(String userName) {
        User result = userDao.checkUserByName(userName);
        if (result != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean saveUser(User user) {
        Integer integer = userDao.saveUser(user);
        return integer!=null;
    }

    @Override
    public boolean updatePassByUsername(User user) {
        Integer integer = userDao.updatePassByUserName(user);
        return integer!=null;
    }
}
