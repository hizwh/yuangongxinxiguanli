package com.zhaowenhao.employee.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;

/**
 * @Description:
 * @Author: zwh
 * @Date: 2019-06-30
 * @Time: 23:33
 */
public class IO {
    /**
     * 保存用户上传图片的路径
     */
    public static String PHOTO_PATH = "C:\\Users\\97506\\IdeaProjects\\employee\\src\\main\\webapp\\head_icon\\";

    public static boolean savePhoto(MultipartFile empPhoto, File file) {
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(empPhoto.getInputStream());
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
            int i = 0;
            while ((i = bufferedInputStream.read()) != -1) {
                System.out.print(i);
                bufferedOutputStream.write(i);
            }
            bufferedOutputStream.close();
            bufferedInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
