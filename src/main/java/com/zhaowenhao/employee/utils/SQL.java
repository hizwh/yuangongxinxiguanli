package com.zhaowenhao.employee.utils;

import org.apache.ibatis.annotations.Param;

/**
 * @Description: SQL操作工具类
 * @Author: zwh
 * @Date: 2019-07-01
 * @Time: 10:40
 */
public class SQL {

    /**
     * 拼接sql语句方法
     *
     * @param empIds
     * @param sql
     * @return
     */
    public static String getSQL(@Param("empIds") String[] empIds, String sql) {
        StringBuilder builder = new StringBuilder();
        builder.append(sql);
        int i = 0;
        for (; i < empIds.length; i++) {
            builder.append(empIds[i]);
            if (i != empIds.length - 1) {
                builder.append(",");
            } else {
                break;
            }
        }
        builder.append(")");
        return builder.toString();
    }
}
