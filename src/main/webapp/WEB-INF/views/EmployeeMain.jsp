<%@ page import="com.zhaowenhao.employee.pojo.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title style="font-size: 20px">员工信息管理系统</title>
    <script src="/layui/layui.all.js"></script>
    <script src="/layui/lay/modules/laydate.js"></script>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <script src="/js/jquery-3.3.1.min.js"></script>
</head>
<%
    String username = (String) session.getAttribute("user");
%>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin layui-bg-blue">
    <div class="layui-header">
        <div class="layui-logo">员工信息管理系统</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="">控制台</a></li>
            <li class="layui-nav-item"><a href="">商品管理</a></li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    <%=username%>
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="#" onclick="updatePassword()">修改密码</a></dd>
                    <dd><a href="/register">账号注册</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="/logout">退出</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item">
                    <a class="" href="javascript:;">员工信息管理</a>
                    <dl class="layui-nav-child">
                        <dd><a onclick="showEmployee()">员工信息</a></dd>
                        <dd><a onclick="addEmployee()">新增员工</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">员工考勤管理</a>
                    <dl class="layui-nav-child">
                        <dd><a onclick="showCheck()">考勤信息</a></dd>
                        <dd><a onclick="addCheck()">添加考勤</a></dd>
                    </dl>
                </li>

                <li class="layui-nav-item">
                    <a class="" href="javascript:;">员工工资信息</a>
                    <dl class="layui-nav-child">
                        <dd><a onclick="showSalary()">员工工资</a></dd>
                        <dd><a onclick="addSalary()">添加工资</a></dd>
                    </dl>
                </li>

                <li class="layui-nav-item">
                    <a class="" href="javascript:;">公司部门信息</a>
                    <dl class="layui-nav-child">
                        <dd><a onclick="showDepartment()">部门信息</a></dd>
                        <dd><a onclick="addDepartment()">添加部门</a></dd>
                    </dl>
                </li>

            </ul>
        </div>
    </div>

    <div class="layui-body" style="max-height: 100%;height: 100%">
        <!-- 内容主体区域 -->
        <img src="../../images/background.png" style="width: 100%;height: 100%;max-width: 100%;max-height: 100%">
    </div>
</div>
<script src="/layui/layui.js"></script>
<script>
    layui.use('element', function () {
        var element = layui.element;
    });

    function addEmployee() {
        $.when(getHtml("/saveEmp")).done(function (html) {
            $(".layui-body").html(html);
        })
    }

    function showEmployee() {
        $.when(getHtml("/showEmp")).done(function (html) {
            $(".layui-body").html(html);
        })
    }

    function addSalary() {
        $.when(getHtml("/saveSalaries")).done(function (html) {
            $(".layui-body").html(html);
        })
    }

    function showSalary() {
        $.when(getHtml("/showSalaries")).done(function (html) {
            $(".layui-body").html(html);
        })
    }

    function addDepartment() {
        $.when(getHtml("/saveDepartment")).done(function (html) {
            $(".layui-body").html(html);
        })
    }

    function showDepartment() {
        $.when(getHtml("/showDepartment")).done(function (html) {
            $(".layui-body").html(html);
        })
    }

    function addCheck() {
        $.when(getHtml("/saveCheck")).done(function (html) {
            $(".layui-body").html(html);
        })
    }

    function showCheck() {
        $.when(getHtml("/showCheck")).done(function (html) {
            $(".layui-body").html(html);
        })
    }

    function updatePassword() {

        layer.open({
            type: 2,
            area: ['377px', '331px'],
            content: "/update_password",
        })
    }

    function getHtml(url) {
        var defer = $.Deferred();
        $.ajax({
            type: "get",
            url: url,
            async: false, // 是否异步
            success: function (html) {
                defer.resolve(html);
            }
        });
        return defer;
    }
</script>
</body>
</html>
