<%@ page import="com.zhaowenhao.employee.pojo.Message" %><%--
  Created by IntelliJ IDEA.
  User: 97506
  Date: 2019/7/3
  Time: 13:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>请登录</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    } </script>
    <meta name="keywords"
          content="Flat Dark Web Login Form Responsive Templates, Iphone Widget Template, Smartphone login forms,Login form, Widget Template, Responsive Templates, a Ipad 404 Templates, Flat Responsive Templates"/>
    <link href="css/style.css" rel='stylesheet' type='text/css'/>
    <!--webfonts-->
    <link href='http://fonts.useso.com/css?family=PT+Sans:400,700,400italic,700italic|Oswald:400,300,700'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.useso.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <script src="../../js/jquery-3.3.1.min.js"></script>
</head>
<body>

<%Message message = (Message) request.getAttribute("message");%>

<%if (message != null) {%>
<script>alert("<%=message.getMessage()%>")</script>
<%}%>

<script>
    $(document).ready(function (c) {
        $('.close').on('click', function (c) {
            $('.login-form').fadeOut('slow', function (c) {
                $('.login-form').remove();
            });
        });
    });
</script>

<div class="login-form">
    <div class="close"></div>
    <div class="head-info">
        <label class="lbl-1"> </label>
        <label class="lbl-2"> </label>
        <label class="lbl-3"> </label>
    </div>
    <div class="clear"></div>
    <div class="avtar">
        <img src="images/avtar.png"/>
    </div>
    <form action="/user" method="post" accept-charset="UTF-8">
        <input name="userName" type="text" class="text" onfocus="this.value = '';"
               placeholder="请输入用户名">
        <div class="key"><input name="userPass" type="password" onfocus="this.value = '';"
                                placeholder="请输入密码">
        </div>
        <div class="signin">
            <input type="submit" value="登录">
        </div>
    </form>
    <a href="/register" style="width: 25px;color: white;font-size: 18px">注册</a>
</div>
</body>
</html>