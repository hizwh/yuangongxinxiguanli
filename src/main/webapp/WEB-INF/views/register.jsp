<%@ page import="com.zhaowenhao.employee.pojo.Message" %><%--
  Created by IntelliJ IDEA.
  User: 97506
  Date: 2019/7/3
  Time: 13:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>请注册</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="../../js/jquery-3.3.1.min.js"></script>
    <script type="application/x-javascript"> addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    } </script>
    <meta name="keywords"
          content="Flat Dark Web Login Form Responsive Templates, Iphone Widget Template, Smartphone login forms,Login form, Widget Template, Responsive Templates, a Ipad 404 Templates, Flat Responsive Templates"/>
    <link href="css/style.css" rel='stylesheet' type='text/css'/>
    <script src="/layui/layui.all.js"></script>

</head>
<body>
<script>
    $(function (c) {
        $('.close').on('click', function (c) {
            $('.login-form').fadeOut('slow', function (c) {
                $('.login-form').remove();
            });
        });
    });
</script>
<%Message message = (Message) request.getAttribute("message");%>

<%if (message != null) {%>
<script>alert("<%=message.getMessage()%>")</script>
<%
    if (message.getCode() == 0) {%>
<script>
    location.href="/main";
</script>
<% }
}
%>
<div class="login-form">
    <div class="close"></div>
    <div class="head-info">
        <label class="lbl-1"> </label>
        <label class="lbl-2"> </label>
        <label class="lbl-3"> </label>
    </div>
    <div class="clear"></div>
    <div class="avtar">
        <img src="images/avtar.png"/>
    </div>
    <form action="/register" method="post" accept-charset="UTF-8">

        <input id="userName" name="userName" type="text" class="text" value="请输入用户名" onfocus="this.value = '';"
               placeholder="请输入用户名">

        <input id="userPass" name="userPass" type="password" onfocus="this.value = '';"
               placeholder="请输入密码" style="margin-top: 30px">
        <input id="userPassAgain" name="userPassAgain" type="password" class="text" onfocus="this.value = '';"
               placeholder="请确认密码" style="margin-top: -50px">

        <div class="register">
            <input type="submit" value="注册">
        </div>
        <a href="/login" style="width: 25px;color: white;font-size: 18px">登录</a>
    </form>
</div>
</body>

</html>
