<%@ page import="com.zhaowenhao.employee.pojo.Check" %><%--
  Created by IntelliJ IDEA.
  User: 97506
  Date: 2019/6/30
  Time: 12:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="/layui/layui.all.js"></script>
<script src="/js/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="/layui/css/layui.css">
<link rel="stylesheet" href="/layui/css/modules/layer/default/layer.css">

<%
    Check check = (Check) request.getAttribute("check");
%>
<style>
    label, dd {
        color: #0C0C0C
    }
    .layui-input{
        width: 350px;
    }
</style>
<form id="form_data" class="layui-form" style="margin-top: 20px">

    <% if (check != null) {%>
    <input type="hidden" name="id" value="<%=check.getId()%>">
    <%}%>
    <div class="layui-form-item">
        <label class="layui-form-label">员工工号</label>
        <div class="layui-input-block">
            <% if (check == null) {%>
            <input type="text" name="empId" required lay-verify="required" placeholder="请输入员工工号"
                   autocomplete="off" class="layui-input" >
            <%} else {%>
            <input readonly type="text" name="empId" required lay-verify="required" placeholder="请输入员工工号"
                   autocomplete="off" class="layui-input" value="<%=check.getEmpId()%>">
            <%}%>
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">考勤日期</label>
        <div class="layui-input-block">

            <% if (check == null) {%>
            <input type="text" name="checkDate" required lay-verify="required" placeholder="请输入考勤日期 1999-07-01 17:50:40"
                   autocomplete="off" class="layui-input" id="date_input">
            <%} else {%>
            <input type="text" name="checkDate" required lay-verify="required" placeholder="请输入考勤日期 1999-07-01 17:50:40"
                   autocomplete="off" class="layui-input" id="date_input" value="<%=check.getCheckDate()%>">
            <%}%>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">考勤类型</label>
        <div class="layui-input-block">
            <select id="check_type" name="checkType" lay-verify="required">
                <option value="事假">事假</option>
                <option value="病假">病假</option>
                <option value="迟到">迟到</option>
                <option value="婚假">婚假</option>
                <option value="加班">加班</option>
                <option value="上下班">上下班</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="button" onclick="sendForm()" class="layui-btn">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    form = layui.form;
    $(function () {
        form.render('select');
        layui.use('laydate', function(){
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#date_input' //指定元素
                ,type: 'datetime'
                ,value: new Date()
                ,min: '1999-1-1'
                ,max: 0
            });
        });
    });


    function sendForm() {
        $.ajax({
            <% if(check == null){%>
            type: "post",
            <%}else{%>
            type: "put",
            <%}%>
            url: "/check",
            data: $("#form_data").serialize(),
            success: function (res) {
                layer.msg(res.message);
                document.getElementById("form_data").reset();
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                parent.layer.close(index); //再执行关闭
            }
        });
    }

</script>
