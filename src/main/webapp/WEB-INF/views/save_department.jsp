<%@ page import="com.zhaowenhao.employee.pojo.Department" %><%--
  Created by IntelliJ IDEA.
  User: 97506
  Date: 2019/6/30
  Time: 12:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="/layui/layui.all.js"></script>
<script src="/js/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="/layui/css/layui.css">
<link rel="stylesheet" href="/layui/css/modules/layer/default/layer.css">
<style>
    label {
        color: #0C0C0C
    }
    .layui-input{
        width: 350px;
    }
</style>
<%
    Department department = (Department) request.getAttribute("department");
%>
<form class="layui-form" id="form_data"  style="margin-top: 20px">
    <div class="layui-form-item">
        <label class="layui-form-label">部门编号</label>
        <div class="layui-input-block">
            <% if (department == null) {%>
            <input type="text" name="departId" required lay-verify="required" placeholder="请输入部门编号" autocomplete="off"
                   class="layui-input">
            <%} else {%>
            <input type="text" name="departId" required lay-verify="required" placeholder="请输入部门编号" autocomplete="off"
                   class="layui-input" value="<%=department.getDepartId()%>">
            <%}%>
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">部门名称</label>
        <div class="layui-input-block">
            <% if (department == null) {%>
            <input type="text" name="departName" required lay-verify="required" placeholder="请输入部门名称" autocomplete="off"
                   class="layui-input">
            <%} else {%>
            <input type="text" name="departName" required lay-verify="required" placeholder="请输入部门名称" autocomplete="off"
                   class="layui-input" value="<%=department.getDepartName()%>">
            <%}%>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="button" class="layui-btn" onclick="sendForm()" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>

    function sendForm() {
        //监听提交
        $.ajax({
            <% if(department == null){%>
            type: "post",
            <%}else{%>
            type: "put",
            <%}%>
            url: "/department",
            data: $("#form_data").serialize(),
            success: function (res) {
                layer.msg(res.message);
                document.getElementById("form_data").reset();
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                parent.layer.close(index); //再执行关闭
            }
        });
    }


</script>
