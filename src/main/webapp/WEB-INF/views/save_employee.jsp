<%@ page import="com.zhaowenhao.employee.pojo.Employee" %><%--
  Created by IntelliJ IDEA.
  User: 97506
  Date: 2019/6/30
  Time: 12:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="/layui/layui.all.js"></script>
<script src="/js/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/layui/css/modules/layer/default/layer.css">

<style>
    label, dd {
        color: #0C0C0C
    }

    .layui-input{
        width: 350px;
    }
</style>
<%
    Employee employee = (Employee) request.getAttribute("emp");
%>

<form id="form_data" class="layui-form" enctype="multipart/form-data" style="margin-top: 20px">
    <% if (employee == null) {%>
    <input type="hidden" name="status" value="save">
    <%} else {%>
    <input type="hidden" name="status" value="update">
    <%}%>
    <div class="layui-form-item">
        <label class="layui-form-label">员工工号</label>
        <div class="layui-input-block">

            <% if (employee == null) {%>
            <input type="text" name="empId" required lay-verify="required" placeholder="请输入员工工号"
                   autocomplete="off" class="layui-input">
            <%} else {%>
            <input readonly type="text" name="empId" required lay-verify="required" placeholder="请输入员工工号"
                   autocomplete="off" class="layui-input" value="<%=employee.getEmpId()%>">
            <%}%>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">员工姓名</label>
        <div class="layui-input-block">
            <% if (employee == null) {%>
            <input type="text" name="empName" required lay-verify="required" placeholder="请输入员工姓名"
                   autocomplete="off" class="layui-input">
            <%} else {%>
            <input type="text" name="empName" required lay-verify="required" placeholder="请输入员工姓名"
                   autocomplete="off" class="layui-input" value="<%=employee.getEmpName()%>">
            <%}%>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">员工部门</label>
        <div class="layui-input-block">
            <select class="depart" name="departId" lay-verify="required" >
            </select>
        </div>
    </div>



    <div class="layui-form-item">
        <label class="layui-form-label">员工年龄</label>
        <div class="layui-input-block">

            <% if (employee == null) {%>
            <input type="text" name="empAge" required lay-verify="required" placeholder="请输入员工年龄"
                   autocomplete="off" class="layui-input">
            <%} else {%>
            <input type="text" name="empAge" required lay-verify="required" placeholder="请输入员工年龄"
                   autocomplete="off" class="layui-input" value="<%=employee.getEmpAge()%>">
            <%}%>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">员工性别</label>
        <div class="layui-input-block">
            <label>男</label><input type="radio" name="empSex" value="男" checked>
            <label>女</label><input type="radio" name="empSex" value="女">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">联系方式</label>
        <div class="layui-input-block">
            <% if (employee == null) {%>
            <input type="text" name="empTel" required lay-verify="required" placeholder="请输入联系方式"
                   autocomplete="off" class="layui-input">
            <%} else {%>
            <input type="text" name="empTel" required lay-verify="required" placeholder="请输入联系方式"
                   autocomplete="off" class="layui-input" value="<%=employee.getEmpTel()%>">
            <%}%>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">家庭住址</label>
        <div class="layui-input-block">

            <% if (employee == null) {%>
            <input type="text" name="empAddr" required lay-verify="required" placeholder="请输入家庭住址"
                   autocomplete="off" class="layui-input">
            <%} else {%>
            <input type="text" name="empAddr" required lay-verify="required" placeholder="请输入家庭住址"
                   autocomplete="off" class="layui-input" value="<%=employee.getEmpAddr()%>">
            <%}%>
        </div>

    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">上传照片</label>
        <div class="layui-input-block">
            <input type="file" name="photo" required lay-verify="required" class="layui-input">
        </div>
    </div>


    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="button" onclick="sendForm()" class="layui-btn">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script>
    form = layui.form;

    getDepartmentInfo();



    function sendForm() {
        var employee = document.getElementById("form_data");
        var formData = new FormData(employee);
        $.ajax({
            type: "post",
            url: "/employee",
            data: formData,
            cache: false,
            async: false,
            processData: false,  //必须false才会避开jQuery对 formdata 的默认处理
            contentType: false,  //必须false才会自动加上正确的Content-Type
            success: function (res) {
                layer.msg(res.message);
                document.getElementById("form_data").reset();
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                parent.layer.close(index); //再执行关闭
            }
        });

    }


    function getDepartmentInfo() {
        $.ajax({
            type: "get",
            url: "/department",
            success: function (res) {
                var data = res.data;
                var select_html = "";
                for (var i = 0; i < data.length; i++) {
                    select_html += "<option  value=\"" + data[i].departId + "\">" + data[i].departName + "</option>";
                }
                console.log(select_html);
                $(".depart").html(select_html);
                form.render('select'); //刷新select选择框渲染
                document.getElementById("form_data").reset();
            }
        })
    }
</script>

