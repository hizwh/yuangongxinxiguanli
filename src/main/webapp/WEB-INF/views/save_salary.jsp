<%@ page import="com.zhaowenhao.employee.pojo.Salary" %><%--
  Created by IntelliJ IDEA.
  User: 97506
  Date: 2019/6/30
  Time: 12:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="/layui/layui.all.js"></script>
<script src="/js/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="/layui/css/layui.css">
<link rel="stylesheet" href="/layui/css/modules/layer/default/layer.css">
<%
    Salary salary = (Salary) request.getAttribute("salary");
%>
<style>
    label {
        color: #0C0C0C
    }

    .layui-input {
        width: 350px;
    }
</style>
<form class="layui-form" id="form_data" style="margin-top: 20px">
    <div class="layui-form-item">
        <label class="layui-form-label">员工工号</label>
        <div class="layui-input-block">

            <% if (salary == null) {%>
            <input type="text" name="empId" required lay-verify="required" placeholder="请输入员工工号"
                   autocomplete="off" class="layui-input">
            <%} else {%>
            <input readonly type="text" name="empId" required lay-verify="required" placeholder="请输入员工工号"
                   autocomplete="off" class="layui-input" value="<%=salary.getEmpId()%>">
            <%}%>

        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">基本工资</label>
        <div class="layui-input-block">
            <% if (salary == null) {%>
            <input type="text" name="empBase" required lay-verify="required" placeholder="请输入基本工资  单位:(元)"
                   autocomplete="off" class="layui-input">
            <%} else {%>
            <input type="text" name="empBase" required lay-verify="required" placeholder="请输入基本工资  单位:(元)"
                   autocomplete="off" class="layui-input" value="<%=salary.getEmpBase()%>">
            <%}%>

        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">员工奖金</label>
        <div class="layui-input-block">

            <% if (salary == null) {%>
            <input type="text" name="empBonus" required lay-verify="required" placeholder="请输入员工奖金  单位:(元)"
                   autocomplete="off" class="layui-input">
            <%} else {%>
            <input type="text" name="empBonus" required lay-verify="required" placeholder="请输入员工奖金  单位:(元)"
                   autocomplete="off" class="layui-input" value="<%=salary.getEmpBonus()%>">
            <%}%>
        </div>
    </div>


    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="button" onclick="sendForm()" class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script>

    function sendForm() {
        //监听提交
        $.ajax({
            <% if(salary == null){%>
            type: "post",
            <%}else{%>
            type: "put",
            <%}%>
            url: "/salary",
            data: $("#form_data").serialize(),
            success: function (res) {
                // layer.open({
                //     anim: 6
                //     ,title: '警告'
                //     ,content: res.message
                // });
                layer.msg(res.message);
                document.getElementById("form_data").reset();
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                parent.layer.close(index); //再执行关闭
            }
        });


    }

</script>
