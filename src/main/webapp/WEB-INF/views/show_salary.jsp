<%--
  Created by IntelliJ IDEA.
  User: 97506
  Date: 2019/6/30
  Time: 14:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>layui在线调试</title>
    <script src="/layui/layui.all.js"></script>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <link rel="stylesheet" href="/layui/css/modules/layer/default/layer.css">
</head>
<body>

<table class="layui-hide" id="demo" lay-filter="test"></table>

<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs layui-btn-radius" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-radius layui-btn-xs" lay-event="del">删除</a>
</script>

<script type="text/html" id="toolbarDemo">
    <input id="search_input" style="width: 200px;height: 30px;" type="text" name="title" required lay-verify="required"
           placeholder="请输入要查询的工号或姓名" autocomplete="off" class="layui-input">
    <button type="button" onclick="search()" class="layui-btn layui-btn-normal layui-btn-radius" lay-event="search">搜索
    </button>
    <button type="button" class="layui-btn layui-btn-danger layui-btn-radius" lay-event="delete">删除</button>
    <button type="button" class="layui-btn layui-btn-warm layui-btn-radius" lay-event="update">修改</button>
</script>


<script>

    layui.use(['laydate', 'laypage', 'layer', 'table', 'carousel', 'upload', 'element', 'slider'], function () {
        laydate = layui.laydate //日期
            , laypage = layui.laypage //分页
            , layer = layui.layer //弹层
            , table = layui.table //表格

        //执行一个 table 实例
        table.render({
            elem: '#demo'
            , url: '/salary' //数据接口
            , title: '用户表'
            , height: 670
            , id: "empId"
            , page: true //开启分页
            , toolbar: '#toolbarDemo' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
            , totalRow: true //开启合计行
            , cols: [[ //表头
                {type: 'checkbox', fixed: 'left'}
                , {field: 'empId', title: '员工工号', width: 200, sort: true, fixed: 'center'}
                , {field: 'empBase', title: '基本工资 单位:(元)', width: 350, sort: true}
                , {field: 'empBonus', title: '奖金 单位:(元)', width: 350, sort: true}
                , {fixed: 'right', width: 500, align: 'center', toolbar: '#barDemo'}
            ]]
            , parseData: function (res) { //res 即为原始返回的数据
                return {
                    "code": res.code, //解析接口状态
                    "msg": res.message, //解析提示文本
                    "count": res.data == null ? 0 : res.data.length, //解析数据长度
                    "data": res.data //解析数据列表
                };
            }
        });

        //监听头工具栏事件
        table.on('toolbar(test)', function (obj) {
            var checkStatus = table.checkStatus(obj.config.id)
                , data = checkStatus.data; //获取选中的数据
            switch (obj.event) {
                case 'update':
                    if (data.length === 0) {
                        layer.msg('请选择一行');
                    } else if (data.length > 1) {
                        layer.msg('只能同时编辑一个');
                    } else {
                        layer.open({
                            type: 2,
                            area: ['500px', '300px'],
                            offset: ['100px', '500px'],
                            title: ['编辑员工薪资', 'font-size:18px;'],
                            content: "/updateSalary?empId=" + data[0].empId, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                            cancel: function (index, layero) {
                                layer.close(index);
                                layer.msg("取消编辑", {
                                    time: 1000
                                })
                            },
                            end: function () {
                                table.reload('empId', {
                                    url: '/salary'
                                });
                            }
                        });
                    }
                    break;
                case 'delete':
                    if (data.length === 0) {
                        layer.msg('请选择一行');
                    } else {
                        layer.confirm('确认删除选中信息吗？', {icon: 2, title: '提示'}, function (index) {
                            var delete_id = "";
                            for (var i = 0; i < data.length; i++) {
                                delete_id += data[i].empId;
                                delete_id += ",";
                            }

                            $.ajax({
                                type: "delete",
                                url: "/salary",
                                data: {deleteId: JSON.stringify(delete_id)},
                                success: function (res) {
                                    console.log(res);
                                    table.reload('empId', {
                                        url: '/salary'
                                    });
                                }
                            });
                            layer.msg('删除成功！');
                        });
                    }
                    break;
            };
        });

        //监听行工具事件
        table.on('tool(test)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            var data = obj.data //获得当前行数据
                , layEvent = obj.event; //获得 lay-event 对应的值
            if (layEvent === 'del') {
                layer.confirm('确认删除该条信息吗？', {icon: 2, title: '提示'}, function (index) {
                    var empId = data.empId;
                    obj.del(); //删除对应行（tr）的DOM结构
                    layer.close(index);
                    $.ajax({
                        type: "delete",
                        url: "/salary/" + empId,
                        success: function (res) {
                            layer.msg('删除成功！');
                        }
                    });
                });
            } else if (layEvent === 'edit') {
                layer.open({
                    type: 2,
                    anim: 1,
                    area: ['500px', '300px'],
                    offset: ['100px', '500px'],
                    title: ['编辑员工薪资', 'font-size:18px;'],
                    content: "/updateSalary?empId=" + data.empId, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                    cancel: function (index, layero) {
                        layer.close(index);
                        layer.msg('取消编辑操作', {
                            time: 1000
                        });
                    },
                    end: function () {
                        table.reload('empId', {
                            url: '/salary'
                        });
                    }
                });
            }
        });


        //分页
        laypage.render({
            elem: 'pageDemo' //分页容器的id
            , count: 100 //总页数
            , skin: '#1E9FFF' //自定义选中色值
            //,skip: true //开启跳页
            , jump: function (obj, first) {
                if (!first) {
                    layer.msg('第' + obj.curr + '页', {offset: 'b'});
                }
            }
        });
    });

    function search() {
        var queryWhere = $("#search_input").val();
        table.reload('empId', {
            url: "/searchSalaries?queryWhere=" + queryWhere
        });
    }
</script>
</body>
</html>