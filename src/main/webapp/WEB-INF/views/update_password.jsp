<%@ page import="com.zhaowenhao.employee.pojo.Message" %><%--
  Created by IntelliJ IDEA.
  User: 97506
  Date: 2019/7/3
  Time: 16:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改密码</title>
    <script src="/layui/layui.all.js"></script>
    <script src="/layui/lay/modules/laydate.js"></script>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <script src="/js/jquery-3.3.1.min.js"></script>
</head
<%Message message = (Message) request.getAttribute("message");%>


<body>
<div class="layui-form-item">
    <form method="post" action="">
        <label class="layui-form-label"><i class="layui-icon">&#xe66f;</i>账号</label>
        <div class="layui-input-inline">
            <input type="text" readonly name="userName" value="<%=session.getAttribute("user")%>" lay-verify="required"  class="layui-input">
        </div>
        <label class="layui-form-label"><i class="layui-icon">&#xe673;</i>旧密码</label>
        <div class="layui-input-inline">
            <input type="password" name="userPass" lay-verify="required" placeholder="请输入旧密码" autocomplete="off" class="layui-input">
        </div>
        <label class="layui-form-label"><i class="layui-icon">&#xe673;</i>新密码</label>
        <div class="layui-input-inline">
            <input type="password" name="newPass" lay-verify="required" placeholder="请输入新密码" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-input-inline">
            <input style="margin: 10px" type="submit" class="layui-btn layui-btn-lg layui-btn-radius layui-btn-normal" value="确认修改">
        </div>

        <div class="layui-input-inline">
            <label id="msg" style="color: red"></label>
        </div>

    </form>

</div>
<%if (message != null) {%>
<script type="text/javascript">layer.msg("<%=message.getMessage()%>")</script>
<%
    if (message.getCode() == 0) {
        session.removeAttribute("user");
%>
<script>
    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
    parent.layer.close(index); //再执行关闭
    window.parent.location.href = "/login";
</script>
<% }
}
%>
</body>
</html>
